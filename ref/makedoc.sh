epydoc --html \
    --docformat=restructuredtext \
    --no-private -v --no-sourcecode \
    --name=CairoIO cairoio.py
epydoc --pdf \
    --docformat=restructuredtext \
    --no-private -v --no-sourcecode \
    --name=CairoIO cairoio.py

import cairo
import cairoio
from gtk import gdk
import gobject
import os
import tempfile

WIDTH = 55
HEIGHT = 100
_, IMAGE_PATH = tempfile.mkstemp()

def setup():
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, WIDTH, HEIGHT)
    pixbuf.save(IMAGE_PATH, 'png')

def teardown():
    try:
        os.remove(IMAGE_PATH)
    except OSError:
        pass

######################################################################
##### Tests for AsyncLoader ##########################################
######################################################################
def test_async_default_values():
    loader = cairoio.AsyncLoader()
    assert loader.get_image_type() == '*'
    assert loader.get_mime_type() == '*'
    assert loader.get_size() == (-1, -1)
    assert not loader.is_aspect_ratio_preserved()

def test_async_set_size():
    loader = cairoio.AsyncLoader()
    loader.set_size(20, 20)
    assert loader.get_size() == (20, 20)

def test_async_set_aspect_ratio():
    loader = cairoio.AsyncLoader()
    loader.set_size(50, -1)
    loader.set_aspect_ratio_preserved(True)
    assert loader.is_aspect_ratio_preserved()

def test_async_set_image_type():
    loader = cairoio.AsyncLoader()
    loader.set_image_type('jpeg')
    assert loader.get_image_type() == 'jpeg'

def test_async_set_mime_type():
    loader = cairoio.AsyncLoader()
    loader.set_mime_type('image/png')
    assert loader.get_mime_type() == 'image/png'

def test_async_bad_aspect():
    '''
    Ensure that a ``RuntimeError`` is raised if both width and height
    are positive or -1, and aspect ratio preserved is ``True``.
    '''
    loader = cairoio.AsyncLoader()
    try:
        loader.set_aspect_ratio_preserved(True)
        assert False
    except RuntimeError:
        assert True

    loader.set_size(30, 40)
    try:
        loader.set_aspect_ratio_preserved(True)
        assert False
    except RuntimeError:
        assert True
    
def test_async_bad_size():
    '''
    Ensure that ValueError is raised if an invalid size is set on the
    loader.
    '''
    loader = cairoio.AsyncLoader()
    try:
        loader.set_size(0, 0)
        assert False
    except ValueError:
        assert True

def test_async_bad_size_for_aspect():
    '''
    Ensure that RuntimeError is raised if setting the size of the
    loader and aspect ratio is preserving leads to an invalid
    combination.
    '''
    loader = cairoio.AsyncLoader()
    loader.set_size(50, -1)
    loader.set_aspect_ratio_preserved(True)
    try:
        loader.set_size(30, 30)
        assert False
    except RuntimeError:
        assert True

def test_async_load_image():
    loader = cairoio.AsyncLoader()
    loader.write(open(IMAGE_PATH).read())
    loader.close()
    surface = loader.get_surface()
    assert surface.get_width() == WIDTH
    assert surface.get_height() == HEIGHT

def test_async_load_animation():
    # Replace path with some animation
    filename = '/home/bjourne/images/kasumicw9.gif'
    loader = cairoio.AsyncLoader()
    loader.write(open(filename).read())
    loader.close()
    frames = loader.get_frames()
    assert len(frames) > 0

def test_async_frame_types():
    '''
    Ensure that the loades frames have the correct types.
    '''
    # Replace path with some animation
    filename = '/home/bjourne/images/kasumicw9.gif'
    loader = cairoio.AsyncLoader()
    loader.write(open(filename).read())
    loader.close()
    frames = loader.get_frames()

    for frame in frames:
        assert len(frame) == 2
        info, delay = frame
        assert isinstance(info, cairoio.SurfaceInfo)
        assert int(delay) == delay

def test_async_load_animation_chunked():
    def get_chunks(string, chunk_size):
        i = 0
        while i < len(string):
            yield string[i:i + chunk_size]
            i += chunk_size

    filename = '/home/bjourne/images/kasumicw9.gif'
    animation = open(filename).read()
    loader = cairoio.AsyncLoader()
    
    for chunk in get_chunks(animation, 512):
        loader.write(chunk)
        anim = loader.get_frames()
        if anim:
            assert len(anim) > 0
    assert len(anim) == 8

def test_async_load_at_size():
    loader = cairoio.AsyncLoader()
    loader.set_size(64, 64)
    loader.write(open(IMAGE_PATH).read())
    loader.close()
    surface = loader.get_surface()
    assert surface.get_width() == 64
    assert surface.get_height() == 64

def test_async_loop():
    '''
    Ensure that the AsyncLoader doesn't accidentally hold something
    open.
    '''
    for x in range(5):
        loader = cairoio.AsyncLoader()
        loader.write(open(IMAGE_PATH).read())
        loader.close()
        surface = loader.get_surface()
        assert surface.get_width() == WIDTH
        assert surface.get_height() == HEIGHT

def test_async_empty_data():
    loader = cairoio.AsyncLoader()
    frames = loader.get_frames()
    assert frames == []
    frames.append('dummy')
    assert loader.get_frames() == frames

def test_async_load_with_aspect():
    '''
    Ensure that the aspect ratio of the image is preserved. Height is
    specified explicitly and width should be resized accordingly.
    '''
    # Can't be tested currently
    return
    loader = cairoio.AsyncLoader()
    loader.set_size(50, -1)
    loader.set_aspect_ratio_preserved(True)
    loader.write(open(IMAGE_PATH).read())
    loader.close()
    surface = loader.get_surface()

    new_height = float(50) / WIDTH * HEIGHT
    assert surface.get_width() == 50
    assert surface.get_height() == new_height

def test_async_close_multiple():
    loader = cairoio.AsyncLoader()
    for x in range(10):
        loader.close()

def test_async_corrupt_data():
    '''
    Ensure that an exception is raised if the loader is fed bad data.
    '''
    loader = cairoio.AsyncLoader()
    try:
        loader.write('x' * 1000)
        loader.close()
        assert False
    except gobject.GError:
        assert True

def test_sig_closed():
    sig_fired = [False]
    def the_cb(loader):
        sig_fired[0] = True
    loader = cairoio.AsyncLoader()
    loader.connect('closed', the_cb)
    loader.close()
    assert sig_fired[0]

def test_sig_size_prepared():
    sig_fired = [False]
    def the_cb(loader, width, height):
        sig_fired[0] = True
    loader = cairoio.AsyncLoader()
    loader.connect('size_prepared', the_cb)
    loader.write(open(IMAGE_PATH).read())
    loader.close()
    assert sig_fired[0]

def test_sig_size_prepared_size():
    sig_fired = [False]
    def the_cb(loader, width, height):
        assert width == WIDTH
        assert height == HEIGHT
        sig_fired[0] = True
        
    loader = cairoio.AsyncLoader()
    loader.connect('size_prepared', the_cb)
    loader.write(open(IMAGE_PATH).read())
    loader.close()
    assert sig_fired[0]

def test_sig_size_prepared_size_with_scaling():
    sig_fired = [False]
    req_size = [0, 0]
    def the_cb(loader, width, height):
        assert width == WIDTH
        assert height == HEIGHT
        sig_fired[0] = True

    for width, height in [(10, 10), (20, 20), (30, 40), (999, 999)]:
        sig_fired = [False]
        req_size[0] = width
        req_size[1] = height
        
        loader = cairoio.AsyncLoader()
        loader.set_size(width, height)
        loader.connect('size_prepared', the_cb)
        loader.write(open(IMAGE_PATH).read())
        loader.close()
        assert sig_fired[0]

def test_sig_area_updated():
    sig_fired = [False]
    def the_cb(loader, x, y, width, height):
        sig_fired[0] = True
    loader = cairoio.AsyncLoader()
    loader.connect('area_updated', the_cb)
    loader.write(open(IMAGE_PATH).read())
    loader.close()
    assert sig_fired[0]

def test_async_set_size_after_sig_size_prepared():
    '''
    Ensure that a RuntimeError is raised if set_size() is called after
    emission of size_prepared.
    '''
    sig_fired = [False]
    def the_cb(loader, width, height):
        sig_fired[0] = True
    loader = cairoio.AsyncLoader()
    loader.connect('size_prepared', the_cb)
    loader.write(open(IMAGE_PATH).read())
    loader.close()
    while not sig_fired[0]:
        pass
    try:
        loader.set_size(100, 100)
        assert False
    except RuntimeError:
        assert True

def test_async_set_size_in_size_prepared():
    '''
    Ensure that set_size() can be called from within the size_prepared
    signal handler.
    '''
    loader = cairoio.AsyncLoader()
    def the_cb(loader, width, height):
        loader.set_size(width * 10, height * 10)
    loader.connect('size_prepared', the_cb)
    loader.write(open(IMAGE_PATH).read())
    loader.close()


######################################################################
##### Tests for the loading functions ################################
######################################################################
def test_load_return():
    surface = cairoio.load(IMAGE_PATH)
    assert isinstance(surface, cairo.ImageSurface)

def test_load():
    surface = cairoio.load(IMAGE_PATH)
    assert surface.get_width() == WIDTH
    assert surface.get_height() == HEIGHT

def test_load_anim():
    info_list = cairoio.load_frames(IMAGE_PATH)

    info, delay = info_list[0]
    surface = info.surface

    assert surface.get_width() == WIDTH
    assert surface.get_height() == HEIGHT
    assert delay == -1

def test_load_at_scale():
    '''
    Ensure that loading frames at width 64 with original height works.
    Note that for this test to work, ``gdk.PixbufLoader`` must be
    fixed.
    '''
    # Can't be tested currently
    return
    info_list = cairoio.load_frames(IMAGE_PATH, width = 64)
    info, delay = info_list[0]
    surface = info.surface
    assert surface.get_width() == 64
    assert surface.get_height() == HEIGHT
    assert delay == -1

def test_load_downsized():
    info_list = cairoio.load_frames(IMAGE_PATH, 15, 17)
    info, delay = info_list[0]
    surface = info.surface
    assert surface.get_width() == 15
    assert surface.get_height() == 17
    assert delay == -1

def test_load_xpm_data():
    data = ["14 14 2 1",
 	    "   c None",
 	    ".  c #000000",
 	    "      ..      ",
 	    "     ....     ",
 	    "    ......    ",
 	    "      ..      ",
 	    "  .   ..   .  ",
 	    " ..   ..   .. ",
 	    "..............",
 	    "..............",
 	    " ..   ..   .. ",
 	    "  .   ..   .  ",
 	    "      ..      ",
 	    "    ......    ",
 	    "     ....     ",
 	    "      ..      "]
    surface = cairoio.load_xpm_data(data).surface
    assert surface.get_width() == 14
    assert surface.get_height() == 14
    

def test_load_bad_xpm():
    try:
        cairoio.load_xpm_data(['hello'])
        assert False
    except IOError:
        assert True

def test_load_empty_xpm():
    # Can't be tested segfaults
    return
    cairoio.load_xpm_data([])

def test_load_alpha_image():
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, True, 8, WIDTH, HEIGHT)

    pixbuf.save('foo.png', 'png')
    
    surface = cairoio.load('foo.png')
    assert surface.get_format() == cairo.FORMAT_ARGB32

    os.remove('foo.png')

def test_load_bad_aspect():
    '''
    Ensure that a ``RuntimeError`` is raised if both width and height
    are positive or -1, and aspect ratio preserved is ``True``.
    '''
    try:
        # Can't preserve aspect ratio if not resizing image at all.
        cairoio.load_frames(IMAGE_PATH, aspect_ratio_preserved = True)
        assert False
    except RuntimeError:
        assert True
    try:
        # Can't preserve aspect ratio if both dimensions are
        # specified.
        cairoio.load_frames(IMAGE_PATH, 10, 10, True)
        assert False
    except RuntimeError:
        assert True

def test_load_bad_size():
    '''
    Ensure that a ValueError is raised if a bad size is specified for
    load_frames().
    '''
    for width, height in [(-100, -100), (0, 0), (100, -100), (0, 10)]:
        try:
            cairoio.load_frames(IMAGE_PATH, width, height)
            assert False
        except ValueError:
            assert True
        

######################################################################
##### Tests for the saving functions #################################
######################################################################
def test_save_image():
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 100, 100)
    cairoio.save(surface, IMAGE_PATH, 'png')
    surface = cairoio.load(IMAGE_PATH)
    assert surface.get_format() == cairo.FORMAT_ARGB32
    assert surface.get_width() == 100
    assert surface.get_height() == 100

def test_save_to_callback():
    # First check how big it is serialized
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 100, 100)
    cairoio.save(surface, IMAGE_PATH, 'jpeg')
    length = len(open(IMAGE_PATH).read())

    callback_len = [0]
    def save_cb(buf, data = None):
        callback_len[0] += len(buf)
    cairoio.save_to_callback(surface, save_cb, 'jpeg')

    assert callback_len[0] == length

def test_save_to_buffer():
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 100, 100)
    cairoio.save(surface, IMAGE_PATH, 'jpeg')
    data1 = open(IMAGE_PATH).read()

    data2 = cairoio.save_to_buffer(surface, 'jpeg')
    assert data1 == data2
    

######################################################################
##### Tests for utility functions ####################################
######################################################################
def test_get_formats():
    for format in cairoio.get_formats():
        assert format['name']
        assert format['description']
        assert format['mime_types']
        assert format['extensions']
        assert format.has_key('is_writable')

def test_get_file_info():
    info = cairoio.get_file_info(IMAGE_PATH)
    surface = cairoio.load(IMAGE_PATH)

    assert len(info) == 3
    info, width, height = info
    assert width == surface.get_width()
    assert height == surface.get_height()
    
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 500, 500)
    cairoio.save(surface, IMAGE_PATH, 'jpeg')

    info, width, height = cairoio.get_file_info(IMAGE_PATH)
    assert width == 500
    assert height == 500
    assert info['name'] == 'jpeg'

    
    

    
    

# -*- coding: utf-8 -*-
'''
This module is the reference implementation for CairoIO. It is a
module that loads and saves images to and from `Cairo
<http://www.cairographics.org/>`__ image surfaces. The module is
intended to be easy to use in combination with Cairo.

CairoIO is supposed to replace the `GdkPixbuf Library
<http://developer.gnome.org/arch/imaging/gdkpixbuf.html>`__ in the
same way that Cairo is supposed to replace large swathes of `GDK
<http://library.gnome.org/devel/gdk/>`__.

This is the reference specification and implementation for the
library. It is implemented purely in Python and is not particularily
fast. It will sooner or later be replaced with a C library.

The only hard dependencies CairoIO has is Cairo, glib and GObject. But
it can take advantage of a large number of format specific image
loaders such as libpng, libjpeg and libtiff.

Scaling
=======
CairoIO can scale image data while it is being loaded. There are two
scaling modes, non-aspect ratio preserving and aspect ratio
preserving.

Non-aspect ratio preserving means that CairoIO scales the width and/or
height of the image data to the specified size. The following code
loads an image into a 64x64 surface.

    .. python::

       surface = cairoio.load_frames('image.png', 64, 64)[0].surface

Aspect ratio preserving means that CairoIO preserves a 1:1 aspect
ratio of the image while it is resizing it. The following code loads
an image 64 pixels high with a width that preserves the aspect ratio:

    .. python::

       surface = cairoio.load_frames('image.png', -1, 64, True)[0].surface

Note that there is no convenience method for scaled loading if you are
only interested in the surface |mdash| you have to use the general
`load_frames()` method. The scaling algorithm choosen is always the
best one available, usually a bilinear sampling.

Converting from gdk.Pixbuf to cairoio
=====================================
Here is a small list of things that are different.

1. Loading an image from a file:

   .. python::

       pixbuf = gdk.pixbuf_new_from_file('filename.png')
       # ==>
       surface = cairoio.load('filename.png')

2. Loading an XPM image and acquiring the 'x_hot' and 'y_hot' options:

   .. python::

      pixbuf = gdk.pixbuf_new_from_xpm_data(data)
      try:
          x_hot = int(pixbuf.get_option('x_hot'))
          y_hot = int(pixbuf.get_option('y_hot'))
      except ValueError:
          x_hot = y_hot = 0
      # ==>
      xpm_info = cairoio.load_xpm_data(data)
      try:
          x_hot = int(xpm_info.options['x_hot'])
          y_hot = int(xpm_info.options['y_hot'])
      except KeyError:
          x_hot = y_hot = 0
      surface = xpm_info.surface

3. Saving a jpeg image with the quality option:

   .. python::

      pixbuf.save('output.jpg', 'jpeg', dict(quality = '100'))
      # ==>
      cairoio.save(surface, 'output.jpeg', 'jpeg', quality = '100')

4. Loading and playing an animation:

    .. python::

       from gtk import gdk 
       import time 
    
       anim = gdk.PixbufAnimation('anim.gif')
       iter = anim.get_iter()
       while True:
           pixbuf = iter.get_pixbuf()
           delay = iter.get_delay_time()
           # Draw pixbuf here
           if delay == -1:
               # -1 is a stop frame
               time.sleep(10)
           else:    
               time.sleep(delay / 1000.0)
           iter.advance()
       # ==>
       import cairoio
       import itertools
       import time

       frames = cairoio.load_frames('anim.gif')
       for info, delay in itertools.cycle(frames):
           # Draw the surface here
           if delay == -1:
               # -1 is a stop frame
               time.sleep(10)
           else:    
               time.sleep(delay / 1000.0)

5. Checking if the image has an alpha channel:

    .. python::

       pixbuf = gdk.pixbuf_new_from_file('image.png')
       print pixbuf.get_has_alpha()
       # ==>
       surface = cairoio.load('image.png')
       print surface.get_format() in (cairoio.ARGB32,)


Needed GdkPixbuf Fixes
======================
Here is a non-exhaustive list of things that must be fixed in
GdkPixbuf for this API to work 100%.

* A way to set the scale size of an existing PixbufLoader.
* A way to set the mime and image type of an existing PixbufLoader.
* A way to list all options in a Pixbuf.

Targeted GdkPixbuf Problems
===========================
GdkPixbuf has some limitations and problems that are hard to overcome.
This API intends to fix some of them:

* Memory waste: GdkPixbuf uses 3+ bytes per pixel. cairoio loads
  images to the most appropriate format available which depends on
  what formats Cairo supports. A 1-bit 5000x5000 image **should** only
  consume 3mb if Cairo is cooperative. `#57183`_, `#142428`_
* Multiple image files: Using `load_frames()` you should be able to
  load for example .ico files containing multiple icions. `#65902`_
* Easier to integrate with Cairo: self-explanatory. `#395578`_, `#491507`_
* More options: Not sure how this should work.. but it should!
  `#143608`_, `#466372`_
* Easier access to animation data. `#358109`_, `#430465`_

.. _#57183: http://bugzilla.gnome.org/show_bug.cgi?id=57183
.. _#65902: http://bugzilla.gnome.org/show_bug.cgi?id=65902
.. _#142428: http://bugzilla.gnome.org/show_bug.cgi?id=142428
.. _#143608: http://bugzilla.gnome.org/show_bug.cgi?id=143608
.. _#358109: http://bugzilla.gnome.org/show_bug.cgi?id=358109
.. _#395578: http://bugzilla.gnome.org/show_bug.cgi?id=395578
.. _#430465: http://bugzilla.gnome.org/show_bug.cgi?id=430465
.. _#466372: http://bugzilla.gnome.org/show_bug.cgi?id=466372
.. _#491507: http://bugzilla.gnome.org/show_bug.cgi?id=491507


Building the Documentation
==========================
This API documentation can be found in the ``./ref`` directory. HTML
and PDF documentation is generated by the script ``makedocs.sh``. To
build the documentation, you need to have the following tools
installed:

* `epydoc <http://epydoc.sourceforge.net>`__ 3.0 beta (install from
  source)
* latex (the packages tetex-bin and tetex-extra in Ubuntu)
* docutils

Then just run the script::

    $ ./makedocs.sh

The source for the documentation is found in the ``./ref/cairoio.py``
file.

Todo
====

* Figure out which exceptions this module should raise. ``IOError``
  makes sense when reading from file, but another error would be more
  appropriate if, for example, an `AsyncLoader` receives corrupt data.
* There should also be an exception for
  ``Loader.set_image_type('mooo')`` or maybe not needed.
* Figure out how loading animations should work.
* Should the loader automagically compensate for embedded orientation?
* Maybe there needs to be a way to explicitly specify what format to
  load images in.

  .. python::

     cairoio.load('image.png', format = cairo.FORMAT_A8)

  for example.

Release history
===============
Major changes between version of CairoIO.

Major changes in svn HEAD
-------------------------
* Renamed load_xpm() to `load_xpm_data()` to make it clear that the
  method does **not** load an .xpm file.

Major changes in 1.0.0
----------------------
Released on 20071116, first release.



:author: `Björn Lindqvist <bjourne@gmail.com>`__
:requires: Python 2.2+, PyGTK, PyCairo
:license: LGPL
:copyright: |copy| 2007 Björn Lindqvist

.. |copy| unicode:: 0xA9 .. copyright sign
.. |mdash| unicode:: 0x02014 .. long dash

:group Loading: load, load_frames, load_xpm_data, load_inline
:group Saving: save, save_to_callback, save_to_buffer
:group Utilities: get_formats, get_file_info
'''

import cairo
import gobject
from gtk import gdk
import time

class SurfaceInfo:
    '''
    SurfaceInfo is the main data object in cairoio. It contains image
    data and information about that image.

    Loading images using caiorio involves going through a SurfaceInfo.

    >>> info = cairoio.load('foobar.png')
    >>> gfx = cairo.Context(info.surface)
    '''
    def __init__(self, surface, options = None):
        '''
        Construct a new SurfaceInfo object.

        :param surface: a ``cairo.ImageSurface``
        :param options: a dict containing key-value option pars.
        '''
        self.surface = surface
        '''A ``cairo.ImageSurface`` with the pixel data for the image.'''
        self.options = options
        '''A Python dictionary with key-value pairs that contains auxilliary
        information about the image.'''

class AsyncLoader:
    '''
    Application driven image and animation loading. AsyncLoader
    provides a way for applications to drive the process of loading an
    image, by letting them send the image data directly to the loader
    instead of having the loader read the data from a file.
    Applications can use this instead of the `load()` or
    `load_frames()` functions when they need to parse image data
    in small chunks. For example, it should be used when reading an
    image from a (potentially) slow network connection, or when
    loading an extremely large file.

    To use AsyncLoader to load an image, just create a new one, and
    call `write()` to send the data to it. When done, the `close()`
    method should be called to end the stream and finalize everything.

    The loader will emit two important signals throughout the process.
    The first, `sig_area_prepared`, will be called as soon as the
    loader has enough information to determine the size of the frames
    to be used. The application can call the `get_frames()` method
    to retrieve the loaded surfaces. No actual image data will be in
    the surfaces, so they can be safely filled with any temporary
    graphics (or an initial color) as needed.

    The `sig_area_updated` signal is emitted every time a region is
    updated. This way you can update a partially completed surface.
    Note that you do not know anything about the completeness of an
    image from the area updated. For example, in an interlaced image,
    you need to make several passes before the image is done loading.

    Loading an Animation
    ====================

    Loading an animation is almost as easy as loading an image. Once
    the first "area-prepared" signal has been emitted, you can call
    the get_animation() method to get the gtk.gdk.PixbufAnimation
    object and the gtk.gdk.PixbufAnimation.get_iter() method to get an
    gtk.gdk.PixbufAnimationIter for displaying it. **this is totally
    wrong**

    :group Read-write properties: set_size, get_size,
        set_mime_type, get_mime_type,
        set_image_type, get_image_type,
        set_aspect_ratio_preserved, is_aspect_ratio_preserved
    :group Signals: sig_area_prepared, sig_area_updated, sig_closed,
        sig_size_prepared
    :group Inherited from GObject: connect
    '''
    def sig_area_prepared(self, user_data = None):
        
        '''
        Emitted when the loader has allocated the surface in the
        desired size. After this signal is emitted, applications can
        call `get_frames()` to fetch the partially-loaded image data.

        :see: `get_frames()`
        '''

    def sig_area_updated(self, x, y, width, height, user_data = None):
        '''
        Emitted when a significant area of the image being loaded has
        been updated. Normally it means that a complete scanline has
        been read in, but it could be a different area as well.
        Applications can use this signal to know when to repaint areas
        of an image that is being loaded.

        :param x: x offset of upper-left corner of the updated area.
        :param y: y offset of upper-left corner of the updated area.
        :param width: width of updated area
        :param height: height of updated area.
        '''

    def sig_closed(self, user_data = None):
        '''
        Emitted when `close()` is called. It can be used by different
        parts of an application to receive notification when an image
        loader is closed by the code that drives it.
        '''

    def sig_size_prepared(self, width, height, user_data = None):
        '''
        Emitted when the loader has been fed the initial amount of
        data that is required to figure out the size of the frames
        that it will create. Applications can call `set_size()` in
        response to this signal to set the desired size to which the
        image should be scaled.

        :param width: the original width of the image
        :param height: the original height of the image
        '''
    
    def __init__(self):
        '''
        Creates a new loader object. The loader is initialized with
        the following default values:

        * *image_type* : "*" (image type is auto detected)
        * *mime_type* : "*" (mime type is auto detected)
        * *size* : (-1, -1)
        * *aspect_ratio_preserved* : ``False``
        '''
        self._pixbuf_loader = gdk.PixbufLoader()
        self.width = -1
        self.height = -1
        self.aspect_ratio_preserved = False
        self.image_type = '*'
        self.mime_type = '*'
        self._frames = []
        self._reload_frames = False
        self._loader_unused = True
        self._fired_sigs = {}
        '''
        Whether we must re-read the frames from ``gdk.PixbufLoader``
        before returning from `get_frames()` the next time it is
        called.
        '''

    def __del__(self):
        '''
        Closes the loader and suppresses any exceptions that may be
        thrown.
        '''
        try:
            self._pixbuf_loader.close()
        except Exception:
            pass

    def get_frames(self):
        '''
        Return the list frames being loaded. The method returns a
        reference to a list of pairs where the first item is a surface
        info object containing the frame data and the second item is
        the frame delay time.

        In general it ony makes sense to call this method after the
        `sig_area_prepared` signal has been emitted by the loader;
        this means that enough data has been read to know the size of
        the surfaces that will be allocated.

        If the loader has not received enough data via `write()`, then
        this method will either return an empty list or a one element
        list whose first `SurfaceInfo` elements ``surface`` attribute
        is ``None``.

        The returned list will be the same in all future calls to the
        loader.

        :return: list of pairs containing a `SurfaceInfo` and frame
            delay for each frame.
        '''
        if self._reload_frames:
            # Load as many frames as possible from the loader.
            #
            # The list of frames is cleared, otherwise multiple calls to
            # write() will cause inconsistent behaviour with duplicated
            # frames.
            while self._frames:
                self._frames.pop()
            anim = self._pixbuf_loader.get_animation()
            if not anim:
                return self._frames
            new_frames = _convert_pixbuf_animation_to_frames(anim)
            for frame in new_frames:
                self._frames.append(frame)
            self._reload_frames = False
            
        return self._frames


    def get_surface(self):
        '''
        Convenience method that returns a reference to the first
        surface. Calling this method is equivalent to:

        >>> frames = loader.get_frames()
        >>> info, delay = frames[0]
        >>> info.surface

        Use this method if the loaded object is an image and you only
        need the surface data and not the image options.

        :return: the currently loading ``cairo.ImageSurface`` or
            ``None``.
        '''
        pixbuf = self._pixbuf_loader.get_pixbuf()
        return _convert_pixbuf_to_surface(pixbuf)

    def write(self, buf, count = -1):
        '''
        Parse the bytes of an image contained in the string specified
        by `buf`. If `count` is specified and is in the range
        ``(0,len(buf))``, only `count` bytes of `buf` are used.
        
        :raises ParseError: if an error occured
        :raises ValueError: if not -1 <= `count` < len(buf)
        '''
        self._pixbuf_loader.write(buf, count)
        self._reload_frames = True
        self._loader_unused = False

    def close(self):
        '''
        Closes the loader and frees its resources. The loader is
        informed that no further writes with `write()` will occur, so
        that it can free its internal loading structures. Also, tries
        to parse any data that hasn't yet been parsed; if the
        remaining data is partial or corrupt, an exception will be
        raised.

        :raises ParseError: if an error occured 
        '''
        if self._loader_unused:
            # Supress the inevitable exception that occurs if no data
            # has been fed to the loader.
            try:
                self._pixbuf_loader.close()
            except gobject.GError:
                pass
        else:
            self._pixbuf_loader.close()

    def set_size(self, width, height):
        '''
        Causes the frames to be scaled while they are loaded. The
        desired frame size can be determined relative to the original
        size of the frames by calling set_size() from a signal handler
        for the `sig_size_prepared` signal. If `width` or `height` is
        set to -1, then the original dimension of the frames will be
        used.

        Attempts to set the frame size after the emission of the
        `sig_size_prepared` signal results in a ``RuntimeError``.

        This code instructs the loader to scale the width to 64 and
        preserve the original height of the image data.

        >>> loader = AsyncLoader()
        >>> loader.set_size(64, -1)

        :param width: the desired width of the frames being loaded or
            -1 to use the original width.
        :param height: the desired height of the frames being loaded
            or -1 to use the original height.
        :raises ValueError: if `width` or `height` is 0 or negative
            and not -1.
        :raises RuntimeError: if *aspect_ratio_preserved* is ``True``
            and **both** `width` and `height` are either -1 or positive.
        :raises RuntimeError: if called after the emission of the
            `sig_size_prepared` signal.
        :see: `set_aspect_ratio_preserved()`
        '''
        # Check if size-prepared has been fired

        # Ensure that width and height are valid
        if width == 0 or width < -1 or height == 0 or height < -1:
            raise ValueError('Width and height must be positive or -1')
        _ensure_sizing_request(width, height, self.aspect_ratio_preserved)

        if self._fired_sigs.get('size_prepared'):
            raise RuntimeError('size cannot be set after emission of ' +
                               'size_prepared')

        self.width = width
        self.height = height

        # Unfortunately, PixbufLoader has no facility for resetting
        # the width and height back to the original size once set.
        # Therefore we have to fake out if width or height is -1.
        if width != -1 and height != -1:
            self._pixbuf_loader.set_size(width, height)

    def get_size(self):
        '''
        Returns the size of the frames being loaded as a tuple. If it
        has not been explicitly specified using `set_size()` then
        (0,0) will be returned if the `sig_area_prepared` signal has
        not been emitted. After that signal has been emitted, the
        returned size will be equal to the size of the frames being
        loaded.

        :return: a 2-tuple containing the width and height of the
            loaded frames.
        '''
        return self.width, self.height

    def set_aspect_ratio_preserved(self, aspect_ratio_preserved):
        '''
        Sets aspect ratio preserving for loading frames. Preserving
        the aspect ratio is only meaningful if the loader has been
        instructed to scale the frames using `set_size()`.

        The aspect ratio will only be preserved if either *width* or
        *height* of the loader has been set to non-zero, but not if
        both dimensions have been set. In all other cases, a
        ``RuntimeError`` will be raised to indicate that the
        combination of *width*, *height* and *aspect_ratio_preserved*
        is invalid. Therefore, `set_size()` must be called before this
        method if a set aspect ratio is desired.

        :param aspect_ratio_preserved: ``True`` to enable aspect ratio
            preserving, ``False`` to disable it.
        :raises RuntimeError: if `aspect_ratio_preserved` is ``True``
            and **both** *width* and *height* are either -1 or positive.
        :raises RuntimeError: if called after the emission of the
            `sig_size_prepared` signal.
        :see: `set_size()`
        '''
        _ensure_sizing_request(self.width, self.height, aspect_ratio_preserved)
        self.aspect_ratio_preserved = aspect_ratio_preserved

    def is_aspect_ratio_preserved(self):
        '''
        Returns whether aspect ratio is preserved.

        :return: ``True`` if the loader preserves the aspect ratio of
            the frames, ``False`` otherwise.
        '''
        return self.aspect_ratio_preserved

    def set_mime_type(self, mime_type):
        '''
        Set the mime type this loader accepts to `mime_type`, instead
        of identifying the type automatically. Useful if you want an
        error if the image or animation isn't the expected mime type,
        for loading image formats that can't be reliably identified by
        looking at the data, or if the user manually forces a specific
        mime type.

        Note that this method is ignored after the emission of the
        `sig_area_prepared` signal.

        The list of supported mime types depends on what image loaders
        are installed, but typically "image/png", "image/jpeg",
        "image/gif", "image/tiff" and "image/x-xpixmap" are among the
        supported mime types. To obtain the full list of supported
        mime types, use:

        >>> formats = cairoio.get_formats()
        >>> mimes = []
        >>> for format in formats:
        >>>     mimes.extend(format.mime_types)

        :param mime_type: the mime type that this loader accepts.
        :see: `get_formats()`, `get_mime_type()`
        '''
        self.mime_type = mime_type

    def get_mime_type(self):
        '''
        Returns the mime type string that this loader accepts or the
        special string "*" indicating that any mime type is
        acceptable.

        :return: a mime type or "*"
        :see: `set_mime_type()`
        '''
        return self.mime_type

    def set_image_type(self, image_type):
        '''
        Set the image type this loader accepts. The loader will always
        attempt to parse data as if it where an image or animation of
        the type `image_type` instead of identifying the type
        automatically. Useful if you want an error if the image isn't
        the expected type, for loading image formats that can't be
        reliably identified by looking at the data, or if the user
        manually forces a specific type.

        Note that this method is ignored after the emission of the
        `sig_area_prepared` signal.

        The list of supported image formats depends on what image
        loaders are installed, but typically "png", "jpeg", "gif",
        "tiff" and "xpm" are among the supported formats. To obtain
        the full list of supported image formats, use:

        >>> types = [format.name for format in cairoio.get_formats()]

        :param image_type: the image type this loader accepts
        :see: `get_image_type()`
        '''
        self.image_type = image_type

    def get_image_type(self):
        '''
        Returns the image type that this loader accepts. The special
        string "*" is returned if the loader isn't limited to one
        particular image or animation type.

        :return: an image type or "*"
        '''
        return self.image_type

    def connect(self, detailed_signal, handler, *args):
        def wrapper(*args, **kwargs):
            # The wrapper makes it so the first param becomes the
            # AsyncLoader instead of the PixbufLoader.
            retval = handler(self, *args[1:], **kwargs)
            self._fired_sigs[detailed_signal] = True
            return retval
        return self._pixbuf_loader.connect(detailed_signal, wrapper, *args)
        

######################################################################
##### Internal functions #############################################
######################################################################
def _convert_pixbuf_to_surface(pixbuf):
    '''
    Convert a ``gdk.Pixbuf`` to a ``cairo.ImageSurface``.
    '''
    format = cairo.FORMAT_RGB24
    if pixbuf.get_has_alpha():
        format = cairo.FORMAT_ARGB32

    width = pixbuf.get_width()
    height = pixbuf.get_height()
    image = cairo.ImageSurface(format, width, height)

    context = cairo.Context(image)
    gdkcontext = gdk.CairoContext(context)
    gdkcontext.set_source_pixbuf(pixbuf, 0, 0)
    gdkcontext.paint()
    return image

def _convert_surface_to_pixbuf(surface):
    '''
    Convert a ``cairo.ImageSurface`` to a ``gdk.Pixbuf``.
    '''
    class CairoToPixbuf(object):
        def __init__(self, surface):
            self.loader = gdk.PixbufLoader()
            surface.write_to_png(self)
            self.loader.close()

        def write(self, data):
            self.loader.write(data)
    return CairoToPixbuf(surface).loader.get_pixbuf()

def _convert_pixbuf_animation_to_frames(anim):
    '''
    Convert a ``gdk.PixbufAnimation`` to a list of frames.
    '''
    if anim.is_static_image():
        return [(SurfaceInfo(anim.get_static_image()), -1)]

    time = 1
    iter = anim.get_iter(time)

    pixbufs = []
    delays = []

    while True:
        pixbuf = iter.get_pixbuf()
        delay = iter.get_delay_time()
        if pixbuf in pixbufs:
            break
        pixbufs.append(pixbuf)
        delays.append(delay)

        time += delay / 1000.0
        while not iter.advance(time):
            time += 0.001
    frames = []
    for pixbuf, delay in zip(pixbufs, delays):
        surface = _convert_pixbuf_to_surface(pixbuf)
        info = SurfaceInfo(surface)
        frames.append((info, delay))
    return frames

def _ensure_sizing_request(width, height, is_aspect):
    '''
    Raises a RuntimeError if the combination of width, height and
    is_aspect is invalid.

    For example, it is impossible to preserve the aspect ratio if both
    width and height have been explicitly set or if both width and
    height is the original dimension of the image data.
    '''
    if is_aspect and ((width > 0 and height > 0) or
                      (width == -1 and height == -1)):
        raise RuntimeError('invalid combination of width, height and aspect ' +
                           'ratio requested')

######################################################################
##### Functions for loading data #####################################
######################################################################
def load(filename):
    '''
    
    Creates a new image surface by loading an image from a file. The
    file format is detected automagically.

    >>> cairoio.load('foobar.png')
    <cairo.ImageSurface object at 0xb71e3e50>

    The most appropriate ``cairo.FORMAT_*`` is choosen automatically.
    For example, if the image files format is 16 bpp, then
    ``cairo.FORMAT_RGB16_565`` is used.

    :param filename: name of file to load, in the GLib file name
        encoding.
    :return: the loaded ``cairo.ImageSurface``.
    :see: `load_frames()`
    '''
    pixbuf = gdk.pixbuf_new_from_file(filename)
    return _convert_pixbuf_to_surface(pixbuf)
    
def load_frames(filename,
                width = -1, height = -1,
                aspect_ratio_preserved = False):
    '''
    Load an image or animation and return all data. This function
    returns a list of pairs where the first item is a surface info
    object containing the frame and the second item is the frame delay
    time. The file format is detected automatically.

    If the file's format does not support multi-frame images, then an
    list with a single surface info will be returned with -1 as delay
    time.

    If `width` or `height` is specified and non-zero, then the images
    will be scaled in either direction to the specified size. If
    `aspect_ratio_preserved` is set to ``True`` then the aspect ratio
    of the scaled image will also be preserved.

    This function should be used both for loading animated images and
    for loading multi-image files such as .ico files.

    .. python::

       frames = cairoio.load_frames('someanim.gif',
                                     width, height,
                                     aspect_ratio_preserved)

    Using this function is equivalent to the following code using
    `AsyncLoader`:

    .. python::

       loader = AsyncLoader()
       loader.set_width(width, height)
       loader.set_aspect_ratio_preserved(aspect_ratio_preserved)
       loader.write(open('someanim.gif').read())
       loader.close()
       frames = loader.get_frames()

    This function can be used by applications in which blocking is
    acceptable while an image is being loaded (small images in
    general). Applications that need progressive loading should use
    AsyncLoader instead.

    :param filename: name of file to load, in the GLib file name
        encoding.
    :param width: the desired width of the frames being loaded.
    :param height: the desired height of the frames being loaded.
    :param aspect_ratio_preserved: ``True`` to enable aspect ratio
            preserving, ``False`` to disable it.
    :return: list of pairs containing the frame delay time and a
        `SurfaceInfo` for the frame.
    :raises IOError: if an error occured while reading the file.
    :raises ParseError: if an error occured while parsing the
        image data.
    :raises RuntimeError: if `aspect_ratio_preserved` is ``True``
        and **both** `width` and `height` are positive or -1.
    :raises ValueError: if `width` or `height` is 0 or negative
        and not -1.    
    :see: `AsyncLoader`
    '''
    loader = AsyncLoader()
    loader.set_size(width, height)
    loader.set_aspect_ratio_preserved(aspect_ratio_preserved)
    loader.write(open(filename).read())
    loader.close()
    return loader.get_frames()

def load_xpm_data(data):
    '''
    Creates a new surface info by parsing XPM data. `data` is a list
    of strings specifying the XPM data to be loaded.

    :param data: a list of strings containing the XPM image data.
    :return: a `SurfaceInfo` with the XPM data.
    '''
    pixbuf = gdk.pixbuf_new_from_xpm_data(data)
    surface = _convert_pixbuf_to_surface(pixbuf)
    info = SurfaceInfo(surface)
    return info

def load_inline(data_length, data, copy_pixels):
    '''
    Creates a surface info from a flat representation that is suitable
    for storing as inline data in a program. This is useful if you
    want to ship a program with images, but don't want to depend on
    any external files.

    GTK+ ships with a program called ``gdk-pixbuf-csource`` which
    allows for conversion of an image into such a inline
    representation. In almost all cases, you should pass the ``--raw``
    flag to ``gdk-pixbuf-csource``. A sample invocation would be::

        gdk-pixbuf-csource --raw --name=myimage_inline myimage.png

    For the typical case where the inline surface is read-only static
    data, you don't need to copy the pixel data unless you intend to
    write to it, so you can pass `False` for `copy_pixels`.

    :param data_length: the length in bytes of the `data`
    :param data: a string containing the inline surface data
    :param copy_pixels: `True` the pixel data should be copied
    :return: a `SurfaceInfo` from the inline data.
    '''

######################################################################
##### Functions for saving data ######################################
######################################################################
def save(surface, filename, type, **opts):
    '''
    Saves an image surface to a file in format type. By default,
    "jpeg", "png", "ico" and "bmp" are possible file formats to
    save in, but more formats may be installed. The list of all
    writable formats can be determined in the following way:

    >>> formats = cairoio.get_formats()
    >>> writable = [format for format in formats format.is_writable()]

    `opts` is a list of key-value strings that modify the save
    parameters. For example:

    >>> cairoio.save(surface, 'file.jpg', 'jpeg', quality = '100')

    Currently only few parameters exist. JPEG images can be saved with
    a "quality" parameter; its value should be in the range [0,100].

    Text chunks can be attached to PNG images by specifying parameters
    of the form "tEXt::key", where key is an ASCII string of length
    1-79. The values are UTF-8 encoded strings. The PNG compression
    level can be specified using the "compression" parameter; it's
    value is in an integer in the range of [0,9].

    ICO images can be saved in depth 16, 24, or 32, by using the
    "depth" parameter. When the ICO saver is given "x_hot" and "y_hot"
    parameters, it produces a CUR instead of an ICO.

    Here is how you can convert an arbitrary image to JPEG, preserving
    the images metadata.

    >>> info, delay = cairoio.load_frames('file.bmp')[0]
    >>> cairoio.save(info.surface, 'file.jpg', 'jpeg', **info.options)

    :param surface: a ``cairo.ImageSurface``
    :param filename: name of file to save.
    :param type: name of file format.
    :param opts: dictionary of key-value options.
    '''
    pixbuf = _convert_surface_to_pixbuf(surface)
    pixbuf.save(filename, type, opts)

def save_to_callback(surface, save_func, type, user_data = None, **opts):
    '''
    Saves the surface in the format specified by type by feeding the
    surfac data to the callback function specified by `save_func`. The
    type may be "jpeg", "png" or "ico" or other installed formats.
    `opts` is a dict containing key-value string pairs that modify the
    save parameters. For example:

    >>> cairoio.save_to_callback(surface, func, 'jpeg', quality = '100')

    If `user_data` is not `None` it is passed to `save_func` with each
    invocation.

    The signature of `save_func` is:

    .. python::
    
        def surface_save_func(buf, data = None):

    where ``buf`` is a string containing the surface data and ``data``
    is `user_data`. ``surface_save_func`` returns `True` if successful
    or `False` on failure.

    :param surface: a ``cairo.ImageSurface``
    :param save_func: a function that is called to save each block of
        data that the save routine generates.
    :param type: the name of the file format.
    :param user_data: user-specified data passed to `save_func` or
        `None`
    :param opts: dictionary of key-value options.
    :see: `save()`
    '''
    pixbuf = _convert_surface_to_pixbuf(surface)
    pixbuf.save_to_callback(save_func, type, opts, user_data)

def save_to_buffer(surface, type, **opts):
    '''
    Saves an image surface to a string with the specified format.
    `type` specifies the image format, which currently is "jpeg",
    "png", "tiff", "ico" or "bmp". `opts` is a dict containing
    key-value string pairs that modify the save parameters. This is a
    convenience function that uses `save_to_callback()` to do
    the real work.

    :param surface: a ``cairo.ImageSurface``
    :param type: the name of the file format.
    :param opts: dictionary of key-value options.
    :return: a string containing the serialized image data.
    :see: `save()`, `save_to_callback()`
    '''
    parts = []
    def add_part(buf, data = None):
        parts.append(buf)
    save_to_callback(surface, add_part, type, **opts)
    return ''.join(parts)


######################################################################
##### Utility functions ##############################################
######################################################################
def get_formats():
    '''
    Obtains the available information about the image formats
    supported by cairoio as a list of dicts. The keys of the image
    format dict are:

     * *name* : the name of the image format.
     * *description* : a description of the image format.
     * *mime_types* : a list of the mime types this image matches.
     * *extensions* : a list of typical filename extensions for the image format.
     * *is_writable* : if `True` the image can be written to a file

    :return: a list of image formats as Python dicts
    '''
    return gdk.pixbuf_get_formats()
    
def get_file_info(filename):
    '''
    Parses an image file far enough to determine its format and size.
    The image information is returned as a 3-tuple where the first
    item is a dict describing the image format and the two other items
    are the images width and height.

    >>> gdk.pixbuf_get_file_info("image.jpg")
    ({'description': 'Bildformatet JPEG',
      'extensions': ['jpeg', 'jpe', 'jpg'],
      'is_writable': True,
      'mime_types': ['image/jpeg'],
      'name': 'jpeg'},
      400,
      301)


    :param filename: the name of the file to identify, in the GLib
        file name encoding.
    :return: a 3-tuple containing image format information and image
        dimensions.
    :raises IOError: if an error occured while reading the file.    
    '''
    return gdk.pixbuf_get_file_info(filename)
